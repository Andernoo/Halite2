#include "ship.hpp"

/// Check if this ship is close enough to dock to the given planet.

bool hlt::Ship::canDock(const Planet & planet, const PlayerId player_id) const {
	return location.get_distance_to(planet.location) <= (constants::SHIP_RADIUS + constants::DOCK_RADIUS + planet.radius) && planet.canDock(player_id);
}

bool hlt::Ship::canAttack(const PlayerId player_id, const std::vector<Ship> allShips) const {
	for (auto ship : allShips) {
		if (ship.owner_id != player_id && this->location.get_distance_to(ship.location) <= hlt::constants::WEAPON_RADIUS)
			return true;
	}
	return false;
}

const std::vector<hlt::Ship> hlt::Ship::getNeighbours(const Map& map, const float distance) const {
	std::vector<Ship> neighbours;
	for (const Ship& potentialNeighbour : map.getAllShips()) {
		if (this->entity_id != potentialNeighbour.entity_id && this->owner_id == potentialNeighbour.owner_id && this->location.get_distance_to(potentialNeighbour.location) <= distance) {
			neighbours.push_back(potentialNeighbour);
		}
	}
	return neighbours;
}
