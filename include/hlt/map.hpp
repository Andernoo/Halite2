#pragma once

#include "map.hpp"
#include "types.hpp"
#include "ship.hpp"
#include "planet.hpp"

namespace hlt {
	struct Ship;
	struct Planet;
    class Map {
    public:
        int map_width, map_height;

        std::unordered_map<PlayerId, std::vector<Ship>> ships;
        std::unordered_map<PlayerId, entity_map<unsigned int>> ship_map;

        std::vector<Planet> planets;
        entity_map<unsigned int> planet_map;

        Map(int width, int height);

		const Ship& get_ship(const PlayerId player_id, const EntityId ship_id) const;

		const Planet& get_planet(const EntityId planet_id) const;

		const std::vector<Ship> getAllShips() const;
    };
}
