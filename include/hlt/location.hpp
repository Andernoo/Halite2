#pragma once

#include <ostream>

#include "constants.hpp"
#include "util.hpp"

namespace hlt {
    struct Location {
        double pos_x, pos_y;

        double get_distance_to(const Location& target) const {
            const double dx = pos_x - target.pos_x;
            const double dy = pos_y - target.pos_y;
            return std::sqrt(dx*dx + dy*dy);
        }

        int orient_towards_in_deg(const Location& target) const {
            return util::angle_rad_to_deg_clipped(orient_towards_in_rad(target));
        }

        double orient_towards_in_rad(const Location& target) const {
            const double dx = target.pos_x - pos_x;
            const double dy = target.pos_y - pos_y;

            return std::atan2(dy, dx) + 2 * M_PI;
        }

        Location get_closest_point(const Location& target, const double target_radius) const {
            const double radius = target_radius + constants::MIN_DISTANCE_FOR_CLOSEST_POINT;
            const double angle_rad = target.orient_towards_in_rad(*this);

            const double x = target.pos_x + radius * std::cos(angle_rad);
            const double y = target.pos_y + radius * std::sin(angle_rad);

            return { x, y };
        }

		Location get_farthest_point(const Location& target, const double target_radius) const {
			const double radius = -target_radius - constants::MIN_DISTANCE_FOR_CLOSEST_POINT;
			const double angle_rad = target.orient_towards_in_rad(*this);

			const double x = target.pos_x + radius * std::cos(angle_rad);
			const double y = target.pos_y + radius * std::sin(angle_rad);

			return { x, y };
		}

		Location() {}
		Location(double x, double y) {
			this->pos_x = x;
			this->pos_y = y;
		}

		static Location subTwoVector(Location v, Location v2) {
			Location tmp;
			tmp.pos_x = (v.pos_x -= v2.pos_x);
			tmp.pos_y = (v.pos_y -= v2.pos_y);
			return tmp;
		}

		void normalize() {
			float m = magnitude();

			if (m > 0) {
				pos_x /= m, pos_y /= m;
			}
		}

		void mulScalar(float scalar) {
			this->pos_x *= scalar;
			this->pos_y *= scalar;
		}

		void divScalar(float scalar) {
			this->pos_x /= scalar;
			this->pos_y /= scalar;
		}

		float magnitude() {
			return sqrt(pos_x * pos_x + pos_y * pos_y);
		}

		bool operator==(const Location& rhs) const {
			return this->pos_x == rhs.pos_x && this->pos_y == rhs.pos_y;
		}

		Location& operator/=(const Location& rhs) {
			this->pos_x /= rhs.pos_x;
			this->pos_y /= rhs.pos_y;
			return *this;
		}

		Location operator+=(const Location& rhs) {
			this->pos_x += rhs.pos_x;
			this->pos_y += rhs.pos_y;
			return *this;

		}

        friend std::ostream& operator<<(std::ostream& out, const Location& location);
    };
}
