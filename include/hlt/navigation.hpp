#pragma once

#include "collision.hpp"
#include "map.hpp"
#include "move.hpp"
#include "util.hpp"
#include "log.hpp"

const double desiredSeparation = hlt::constants::SHIP_RADIUS * 10;

namespace hlt {
	namespace navigation {
		static Location separation(Ship ship, std::vector<Ship> ships) {
			hlt::Log::log("[Navigation] Ship " + std::to_string(ship.entity_id) + " calculating separation vector for " + std::to_string(ships.size()) + " ships");
			// Distance of field of vision for separation between boids
			Location steer(0, 0);
			int count = 0;
			// For every boid in the system, check if it's too close
			for (auto neighbour : ships) {
				// Calculate distance from current boid to boid we're looking at
				float d = ship.location.get_distance_to(neighbour.location);
				if ((d > 0) && (d < desiredSeparation)) {
					Location diff(0, 0);
					diff = Location::subTwoVector(ship.location, neighbour.location);
					diff.normalize();
					diff.divScalar(d);      // Weight by distance
					steer += diff;
					count++;
				}
			}
			// Adds average difference of location to acceleration
			if (count > 0)
				steer.divScalar((float)count);
			if (steer.magnitude() > 0) {
				// Steering = Desired - Velocity
				steer.normalize();
				steer.mulScalar(constants::MAX_SPEED);
			} 
			hlt::Log::log("[Navigation] Separation vector is " + std::to_string(steer.pos_x) + ", " + std::to_string(steer.pos_y));
			return steer;
		}

		static Location flock(const Ship& ship, const Map& map) {
			Location sep = separation(ship, ship.getNeighbours(map));

			return sep;
		}

		static void check_and_add_entity_between(std::vector<const Entity *>& entities_found, const Location& start, const Location& target, const Entity& entity_to_check) {
			const Location &location = entity_to_check.location;
			if (location == start || location == target) {
				return;
			}
			if (collision::segment_circle_intersect(start, target, entity_to_check, constants::FORECAST_FUDGE_FACTOR)) {
				entities_found.push_back(&entity_to_check);
			}
		}

		static std::vector<const Entity *> objects_between(const Map& map, const Location& start, const Location& target) {
			std::vector<const Entity *> entities_found;

			for (const Planet& planet : map.planets) {
				check_and_add_entity_between(entities_found, start, target, planet);
			}

			for (const auto& player_ship : map.ships) {
				for (const Ship& ship : player_ship.second) {
					check_and_add_entity_between(entities_found, start, target, ship);
				}
			}

			return entities_found;
		}

		static possibly<Move> navigate_ship_towards_target(
			const Map& map,
			const Ship& ship,
			const Location& target,
			const int max_thrust,
			const bool avoid_obstacles,
			const int max_corrections = hlt::constants::MAX_NAVIGATION_CORRECTIONS,
			const double angular_step_rad = M_PI / 180.0)
		{
			if (max_corrections <= 0) {
				return { Move::noop(), false };
			}

			double distance = ship.location.get_distance_to(target);
			double angle_rad = ship.location.orient_towards_in_rad(target);

			if (avoid_obstacles && !objects_between(map, ship.location, target).empty()) {
				const double new_target_dx = cos(angle_rad + angular_step_rad) * distance;
				const double new_target_dy = sin(angle_rad + angular_step_rad) * distance;
				const Location new_target = { ship.location.pos_x + new_target_dx, ship.location.pos_y + new_target_dy };

				return navigate_ship_towards_target(map, ship, new_target, max_thrust, true, (max_corrections - 1), angular_step_rad);
			}

			Location targetVector(distance * std::cos(angle_rad), distance * std::sin(angle_rad));
			Location flockVector = flock(ship, map);
			//targetVector += flockVector;
			hlt::Log::log("Target Vector: " + std::to_string(targetVector.pos_x) + " " + std::to_string(targetVector.pos_y));
			hlt::Log::log("Flock Vector: " + std::to_string(flockVector.pos_x) + " " + std::to_string(flockVector.pos_y));

			//distance = ship.location.get_distance_to(targetVector);
			int thrust;
			if (distance < max_thrust) {
				// Do not round up, since overshooting might cause collision.
				thrust = (int)distance;
			} else {
				thrust = max_thrust;
			}

			const int angle_deg = util::angle_rad_to_deg_clipped(angle_rad);

			return { Move::thrust(ship.entity_id, thrust, angle_deg), true };
		}

		static possibly<Move> orbitTarget(const Map& map, const Ship& ship, const Entity& orbitTarget, int maxThrust = hlt::constants::MAX_SPEED, const bool avoidObstacles = true) {
			if (ship.location.get_distance_to(orbitTarget.location) <= (orbitTarget.radius + maxThrust)) {
				const double angle = atan2(orbitTarget.location.pos_y - ship.location.pos_y, orbitTarget.location.pos_x - ship.location.pos_x) + M_PI / 4;
				const Location newTarget = {
					(orbitTarget.radius + maxThrust) * cos(angle) + orbitTarget.location.pos_x,
					(orbitTarget.radius + maxThrust) * sin(angle) + orbitTarget.location.pos_y
				};

				if (ship.canAttack(ship.owner_id, map.getAllShips()))
					maxThrust = 0;

				return navigate_ship_towards_target(map, ship, newTarget, maxThrust, avoidObstacles);
			} else {
				return navigate_ship_towards_target(map, ship, orbitTarget.location, maxThrust, avoidObstacles);
			}
		}
	}
}
