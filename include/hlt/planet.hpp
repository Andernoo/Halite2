#pragma once

#include <vector>

#include "types.hpp"
#include "entity.hpp"
#include "log.hpp"
#include "ship.hpp"

namespace hlt {
	struct Ship;

	struct Planet : Entity {
		bool owned;

		/// The remaining resources.
		int remaining_production;

		/// The currently expended resources.
		int current_production;

		/// The maximum number of ships that may be docked.
		unsigned int docking_spots;

		/// Contains IDs of all ships in the process of docking or undocking,
		/// as well as docked ships.
		std::vector<EntityId> docked_ships;

		const bool isFull() const;
		
		const bool isMine(PlayerId player_id) const;

		const bool canDock(PlayerId player_id) const;

		const bool isUnderAttack(PlayerId player_id, const std::vector<Ship> allShips) const;

		double normalise(const double x, double in_min, double in_max, double out_min, double out_max) const;

		double getValue(const std::vector<Ship> allShips, Location location, PlayerId player_id, double max_distance, double min_planet_size, double max_planet_size) const;
	};
}
