#include "map.hpp"

namespace hlt {
    Map::Map(const int width, const int height) : map_width(width), map_height(height) {
    }
	const Ship & Map::get_ship(const PlayerId player_id, const EntityId ship_id) const {
		return ships.at(player_id).at(ship_map.at(player_id).at(ship_id));
	}
	const Planet & Map::get_planet(const EntityId planet_id) const {
		return planets.at(planet_map.at(planet_id));
	}
	const std::vector<Ship> Map::getAllShips() const {
		std::vector<Ship> allShips;
		for (auto ship : ships) {
			allShips.insert(allShips.end(), ship.second.begin(), ship.second.end());
		}
		return allShips;
	}
}
