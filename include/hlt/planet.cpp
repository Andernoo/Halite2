#include "planet.hpp"

const static double CLAIM_WEIGHT 	= 4.2;
const static double BOLSTER_WEIGHT 	= 4.1;
const static double DEFEND_WEIGHT 	= 4;
const static double ATTACK_WEIGHT 	= 4.1;
const static double RADIUS_WEIGHT 	= 0.9;
const static double DISTANCE_WEIGHT	= 1.0;

const bool hlt::Planet::isFull() const {
	return docked_ships.size() == docking_spots;
}

const bool hlt::Planet::isMine(PlayerId player_id) const {
	return this->owner_id == player_id;
}

const bool hlt::Planet::canDock(PlayerId player_id) const {
	return !this->owned || (this->isMine(player_id) && !this->isFull());
}

const bool hlt::Planet::isUnderAttack(PlayerId player_id, const std::vector<Ship> allShips) const {
	if (!owned || owner_id != player_id)
		return false;
	 for (const auto ship : allShips) {
	 	if (ship.owner_id != player_id && location.get_distance_to(ship.location) < (2 * radius))
	 		return true;
	 }
	return false;
}

double hlt::Planet::normalise(const double x, double in_min, double in_max, double out_min, double out_max) const {
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

double hlt::Planet::getValue(const std::vector<Ship> allShips, Location location, PlayerId player_id, double max_distance, double min_planet_size, double max_planet_size) const {
	double value = 0;
	value += CLAIM_WEIGHT		* (!this->owned);
	value += BOLSTER_WEIGHT		* (this->isMine(player_id) && !this->isFull());
	value += DEFEND_WEIGHT		* (this->isUnderAttack(player_id, allShips));
	value *= RADIUS_WEIGHT 		* normalise(this->radius, min_planet_size, max_planet_size, 1.0, 2.0);			
	
	value += ATTACK_WEIGHT		* (!this->isMine(player_id));
	value *= DISTANCE_WEIGHT	* std::pow(normalise(this->location.get_distance_to(location), 0, max_distance, 0.0, 1.0), -2);
	return value;
}
