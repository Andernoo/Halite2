#include "include/hlt/hlt.hpp"
#include "include/hlt/navigation.hpp"
#include "include/hlt/log.hpp"
#include <algorithm>
#include <vector>

// Config Loading
//#include "include/json/json.h"

// Neural Network
//#include "include/NeuralNetwork/NeuralNetwork.h"
//#include <iostream>

// TODO
// radius affects bolster more (affected by distance)
// Undock to defend against enemies?
// Prevent suicide/collision at the start - flocking
// Suicide bomb if enemy planet too strong?
// Prevent zerg-rushes...
// zerg rush...
// Hide in the (closest) corner?
// Attack early (2 player match)
// suicide if planet surrounded
// Study this: https://halite.io/play/?game_id=2760393&replay_class=0&replay_name=replay-20171116-063349%2B0000--3143961885-336-224-1510813724

int main() {
	const hlt::Metadata metadata = hlt::initialize("AnooBot-9");
	const hlt::PlayerId player_id = metadata.player_id;
	const hlt::Map& initial_map = metadata.initial_map;

	// We now have 1 full minute to analyse the initial map.
	std::ostringstream initial_map_intelligence;
	initial_map_intelligence
		<< "width: " << initial_map.map_width
		<< "; height: " << initial_map.map_height
		<< "; players: " << initial_map.ship_map.size()
		<< "; my ships: " << initial_map.ship_map.at(player_id).size()
		<< "; planets: " << initial_map.planets.size();
	hlt::Log::log(initial_map_intelligence.str());

	const double max_distance = std::sqrt(std::pow(initial_map.map_width, 2) + std::pow(initial_map.map_height, 2));
	const double max_planet_size = [&]() -> const double {
		double max_planet_size = 0;
		for (auto planet : initial_map.planets) {
			if (planet.radius > max_planet_size)
				max_planet_size = planet.radius;
		}
		return max_planet_size;
	}();

	const double min_planet_size = [&]() -> const double {
		double min_planet_size = 999;
		for (auto planet : initial_map.planets) {
			if (planet.radius < min_planet_size)
				min_planet_size = planet.radius;
		}
		return min_planet_size;
	}();

	// Create neural network
	//BPN::Network::Settings networkSettings{ 5, 3, 1 };
	//BPN::Network nn( networkSettings );

	std::vector<hlt::Move> moves;
	for (;;) {
		moves.clear();
		const hlt::Map map = hlt::in::get_map();
		std::vector<hlt::Planet> planets = map.planets;
		int shipCount = 0;

		for (const hlt::Ship& ship : map.ships.at(player_id)) {
			//hlt::Log::log("[Behaviour] Ship " + std::to_string(shipCount++));
			if (ship.docking_status != hlt::ShipDockingStatus::Undocked) {
				continue;
			}

			// Main Priority Sorting Function (per ship)
			std::sort(planets.begin(), planets.end(), [&](const hlt::Planet& a, const hlt::Planet& b) -> bool {
				return a.getValue(map.getAllShips(), ship.location, player_id, max_distance, min_planet_size, max_planet_size) > b.getValue(map.getAllShips(), ship.location, player_id, max_distance, min_planet_size, max_planet_size);
			});

			for (int i = 0; i < std::min(3, (int)planets.size()); i++) {
				//hlt::Log::log(std::to_string(planets[i].getValue(map.getAllShips(), ship.location, player_id, max_distance, min_planet_size, max_planet_size)));
			}

			for (const hlt::Planet& planet : planets) {
				hlt::possibly<hlt::Move> move; 
				if (planet.owned && planet.owner_id != player_id) {
					move = hlt::navigation::orbitTarget(map, ship, planet, hlt::constants::MAX_SPEED, true);
					if (move.second) {
						hlt::Log::log("[Behaviour] Ship " + std::to_string(ship.entity_id) + " is attacking");
						moves.push_back(move.first);
						break;
					}
					hlt::Log::log("[Behaviour] Ship " + std::to_string(ship.entity_id) + " FAILED to attack");
				} else {
					if (ship.location.get_distance_to(planet.location) <= (planet.radius + hlt::constants::DOCK_RADIUS)) {
						if (planet.isUnderAttack(player_id, map.getAllShips())) {
							move = hlt::navigation::orbitTarget(map, ship, planet, hlt::constants::MAX_SPEED);
						} else {
							if (ship.canDock(planet, player_id)) {
								hlt::Log::log("[Behaviour] Ship " + std::to_string(ship.entity_id) + " is docking");
								moves.push_back(hlt::Move::dock(ship.entity_id, planet.entity_id));
								break;
							}
						}
					}

					move = hlt::navigation::orbitTarget(map, ship, planet, hlt::constants::MAX_SPEED);
					if (move.second) {
						hlt::Log::log("[Behaviour] Ship " + std::to_string(ship.entity_id) + " is moving to dock");
						moves.push_back(move.first);
						break;
					}
					hlt::Log::log("[Behaviour] Ship " + std::to_string(ship.entity_id) + " FAILED to move to dock");

					move = hlt::navigation::orbitTarget(map, ship, planet, hlt::constants::MAX_SPEED, true);
					if (move.second) {
						hlt::Log::log("[Behaviour] Ship " + std::to_string(ship.entity_id) + " is defending");
						moves.push_back(move.first);
						break;
					}
					hlt::Log::log("[Behaviour] Ship " + std::to_string(ship.entity_id) + " FAILED to defend");
				}

				hlt::Log::log("[Behaviour] Ship " + std::to_string(ship.entity_id) + " is IDLING");
				break;
			}
		}

		if (!hlt::out::send_moves(moves)) {
			hlt::Log::log("send_moves failed; exiting");
			break;
		}
	}
}